#!/bin/gawk

BEGIN {
    if (old) {
        lua = "lua tr.lua " old " " lang
        while(( lua | getline line ) > 0 ) {
            if (match(line, /^( *)\|(.*)$/, a)) {
                l = length(a[1])/2
            print(line)>/dev/stderr
                if (l < 3)
                    last[l] = a[2]
                else
                    so[last[0]][last[1]] = a[2]
            }
        }
        close(lua)
    }
}
{
    r0=$0
    if (skip && match(r0, /^.*\]\](.*)$/, rr)) {
        r0 = rr[1]
        skip=null
    }
    if (!skip) {
        r1=""
        while ( match(r0, /^((("(\\.|[^"\\])*")*|([^"-]|-[^-])*)+)(--(([^\[].*)|(\[\[([^\]|][^\]])*\]\](.*))|(\[\[([^\]]|(][^\]]))*)))?$/, rr) ) {
            r0=rr[11]
            r1=r1 rr[1] " "
            if (rr[12]) skip=1
            if (!r0) break
        }
        r = r1
        while( match(r, /tr\s*\(\s*("(\\"|[^"])*")\s*\)(.*)/, a) ) {
            if (!so[a[1]][1])
                s[a[1]][1] = a[1]
            else
                s[a[1]][1] = so[a[1]][1]
            r=a[3]
        }
        r=r1
        while( match(r, /tr\s*\(\s*("(\\"|[^"])*")\s*,\s*("(\\"|[^"])*")(\s*,\s*[^\)[:space:]]*)?\s*\)(.*)/, a) ) {
            if (!so[a[1]][a[3]])
                s[a[1]][a[3]] = a[1]
            else {
                s[a[1]][a[3]] = so[a[1]][a[3]]
            }
            r=a[6]
        }
    }
}
END {
    for (i in s) {
        printf("    [%s] = {\n", i)
        for (j in s[i]) {
            printf("        [%s] = { %s },\n", j, s[i][j])
        }
        print("    },")
    }
}
