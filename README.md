# Strategos

Strategos enhances your battleground experience and gameplay.

Features
---------

* General
  - Skip Battlebaster gossip
  - Battleground start timer
  - Spirit Healer timer
* Warsong Gulch
  - Flag carrier frames featuring:
    + Name
    + Health bar
    + Class icon
    + Click to target
    + Flag reset timer
* Alterac Valley
  - Contested nodes timers (click to announce in chat)
 

Thanks
-------
Jensen for german localization.
Nixxen#3158 (Discord) for assisting Arathi Basin development.
 
## FAQ

### Strategos doesn't show up in the addon list

- Be sure to have the uncompressed folder inside `_classic_\Interface\AddOns` folder
- Be sure that the folder name is correcly named `Strategos` (and not `Strategos-master`)
- When everythig's fine, this file should exist `_classic_\Interface\AddOns\Strategos\Strategos.toc`


### Strategos complains about missing dependency

Install [EmeraldFramework](https://gitlab.com/woblight/EmeraldFramework)


### The flag carrier button in Warsong Gulch targets the wrong player

Due to API limitations, the target of the button cannot change while you are in combat. The name of the flag carrier is grey when it doesn't match the target fo the button.


### Battleground start timer is late by a couple of seconds

Blizzard's own countdown is wrong. The timer matches Blizzard's chat announcemnt.


### Warsong Gulch flag reset timer is not accurate

All timers have an around 5 second random offset.


### Spirit Healer timer is not accurate

The first resurrection has a random time offset. The timer will be accurate after the first time you see the Spirit Healer.


### The graveyard displayed in Alterac Valley does not metch the nearest graveyard

Due to API limitations, it's not possible to get you position in the map, thus the graveyard is detected based on your last visited graveyard.

## Website
 
https://woblight.gitlab.io
