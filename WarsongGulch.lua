local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

EFrame.newClass("WarsongGulch", Battleground)

WarsongGulch.id = WOW_PROJECT_ID == WOW_PROJECT_MAINLINE and 2106 or 489

idLookups[WarsongGulch.id] = WarsongGulch

WarsongGulch:attach("enemyFlagCarrier")
WarsongGulch:attach("alliedFlagCarrier")
WarsongGulch:attachSignal("restarting")
WarsongGulch.scan = true

local ressoff = 0

function WarsongGulch:spiritHealerTime()
    return tresstime - (GetTime() - self.bgtime - 132.5 + ressoff)%tresstime
end


local carriersIds = {"arena1", "arena2"}

function WarsongGulch:new(debug)
    Battleground.new(self)
    self.flags = {Flag(self), Flag(self)}
    self.carriers = {Carrier(self), Carrier(self)}
    self.flags[Alliance].carrier = self.carriers[Horde]
    self.flags[Horde].carrier = self.carriers[Alliance]
    self.carriers[Horde].faction = Horde
    self.carriers[Alliance].faction = Alliance
    self.l1 = FlagFrame(self, self.flags[Horde], Alliance, 2)
    self.l2 = FlagFrame(self, self.flags[Alliance], Horde, 1)
    self.l1.width = EFrame.bind(function() return math.max(self.l1.implicitWidth, self.l2.implicitWidth) end)
    self.l2.width = EFrame.bind(function() return math.max(self.l1.implicitWidth, self.l2.implicitWidth) end)
    self.l1.marginLeft = EFrame.bind(function() return EFrame.normalizeUI(EFrame.normalize(60)) end)
    self.l2.marginLeft = EFrame.bind(function() return EFrame.normalizeUI(EFrame.normalize(60)) end)
    local anchorFrame = debug or UIWidgetTopCenterContainerFrame
    self.l1.anchorTopLeft = {frame = anchorFrame, point = "TOPRIGHT"}
    self.l1.height = EFrame.bind(function() return anchorFrame:GetHeight() * 0.42 * UIParent:GetScale() / EFrame.root.scale end)
    self.l2.anchorBottomLeft = {frame = anchorFrame, point = "BOTTOMRIGHT"}
    self.l2.height = EFrame.bind(function() return anchorFrame:GetHeight() * 0.42 * UIParent:GetScale() / EFrame.root.scale end)
    self:connect("restarting", self.startTimer, "restart")
    self.b = SpiritHealerIndicator()
    self.b.anchorTopLeft = {frame = anchorFrame, point = "BOTTOMRIGHT"}
    
    if WOW_PROJECT_ID ~= WOW_PROJECT_CLASSIC then
        EventHandler:RegisterEvent("ARENA_OPPONENT_UPDATE")
        function EventHandler.ARENA_OPPONENT_UPDATE(n, s)
            local i = n == "arena2" and Alliance or n == "arena1" and Horde
            if i then
                local flag = self.flags[3 - i]
                if s ~= "cleared" then
                    flag:pick(n)
                else
                    if flag.status == Flag.Carried then
                        flag:drop()
                    end
                end
            end
        end
        for k,v in ipairs(carriersIds) do
            local flag = self.flags[k]
            if UnitExists(v) then
                flag:pick(v)
            end
        end
    end
        
end

function WarsongGulch:destroy()
    if WOW_PROJECT_ID ~= WOW_PROJECT_CLASSIC then
        EventHandler:UnregisterEvent("UNIT_NAME_UPDATE")
    end
    self.l1:destroy()
    self.l2:destroy()
    self.b:destroy()
    Battleground.destroy(self)
end


local evs = {
    [tr("Warsong Gulch.- 1 minute")] = function (self)
        self.status = Battleground.Starting
        self:starting(60)
        self.scan = false
    end,
    [tr("Warsong Gulch.- 30 seconds")] = function (self)
        self.status = Battleground.Starting
        self:starting(30)
        self.scan = false
    end,
    [tr("^Let the battle for Warsong Gulch begin!")] = function (self)
        self.status = nil
        self:started()
        self.scan = false
    end,
    [tr("picked")] = function (self, message, faction)
        if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
            _,_,name = strfind(message, tr("by (.+)!"))
            local flag = self.flags[3 - faction]
            flag:pick(name)
            if faction == playerFaction then
                self.scan = false
            end
        end
    end,
    [tr("dropped")] = function (self, message, faction)
        local flag = self.flags[faction]
        flag:drop()
        if faction ~= playerFaction then
            self.scan = false
        end
    end,
    [tr("returned")] = function (self, message, faction)
        local flag = self.flags[faction]
        flag:place()
        if faction ~= playerFaction then
            self.scan = false
        end
    end,
    [tr("captured")] = function (self)
        self.flags[Alliance]:reset()
        self.flags[Horde]:reset()
        self:restarting(10)
        self.status = self.Starting
        self.scan = false
    end,
    [tr("placed")] = function (self, message, faction)
        if strfind(message, tr("Horde")) then
            self.flags[Horde]:place()
            if faction == Alliance then
                self.scan = false
            end
        elseif strfind(message, tr("Alliance")) then
            self.flags[Alliance]:place()
            if faction == Horde then
                self.scan = false
            end
        else
            self.flags[Alliance]:place()
            self.flags[Horde]:place()
            self.status = nil
            self.scan = false
        end
    end
}

function WarsongGulch:processChatEvent(message, faction)
    for s,f in pairs(evs) do
        if strfind(message, s) then
            f(self, message, faction)
            return
        end
    end
    Battleground.processChatEvent(self, message, faction)
end

function WarsongGulch:updateScore()
    Battleground.updateScore(self)
    for i=1,2 do
        if self.carriers[i].name and not self.carriers[i].class then
            self.carriers[i].class = self:playerClass(self.carriers[i].name)
        end
    end
    if not self.b.ok then
        self.b:restart(self)
        self.b.ok = true
    end
end

local oldAllied
local oldAlliedTime
local oldEnemy
local oldEnemyTime

local lastSpiritHealerTime = 0

function WarsongGulch:update()
    if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
        local alliedFlag = self.flags[3 - playerFaction]
        local enemyFlag = self.flags[playerFaction]
        local name, name2 = alliedFlag.carrier.__name, enemyFlag.carrier.__name
        if oldAllied and GetUnitName(oldAllied, true) ~= name then
            oldAllied = nil
        end
        if oldEnemy and GetUnitName(oldEnemy, true) ~= name2 then
            oldEnemy = nil
        end
        if self.scan and not oldAllied and (not oldAlliedTime or GetTime() - oldAlliedTime > 1) then
            oldAllied = nil
            oldAlliedTime = GetTime()
            for i = 1, GetNumGroupMembers() do
                local u = "raid" .. i
                local k = 1
                local bn = UnitBuff(u, k)
                while bn do
                    if bn == tr("Alliance Flag") or bn == tr("Horde Flag") then
                        oldAllied = u
                        oldAlliedTime = nil
                        if GetUnitName(u, true) ~= name then
                            self.flags[3 - playerFaction]:pick(GetUnitName(u, true))
                        end
                        break
                    end
                    k = k + 1
                    bn = UnitBuff(u, k)
                end
            end
        end
        if alliedFlag.status == Flag.Carried and name and not oldAllied and (not oldAlliedTime or GetTime() - oldAlliedTime > 1) then
            oldAllied = nil
            oldAlliedTime = GetTime()
            for i = 1, GetNumGroupMembers() do
                local u = "raid" .. i
                if name == GetUnitName(u, true) then
                    oldAllied = u
                    oldAlliedTime = nil
                    break
                end
            end
        end
        if name2 and not oldEnemy and (not oldEnemyTime or GetTime() - oldEnemyTime > 1) then
            oldEnemy = nil
            oldEnemyTime = GetTime()
            for i = 1, GetNumGroupMembers() do
                local u = "raid" .. i .. "target"
                if GetUnitName(u, true) == name2 then
                    oldEnemy = u
                    oldEnemyTime = nil
                    break
                end
            end
        end
        if oldAllied then
            self.carriers[playerFaction].health = UnitHealth(oldAllied)/UnitHealthMax(oldAllied) * 100
        else
            self.carriers[playerFaction].health = nil
        end
        if oldEnemy then
            self.carriers[3 - playerFaction].health = UnitHealth(oldEnemy)/UnitHealthMax(oldEnemy) * 100
        else
            self.carriers[3 - playerFaction].health = nil
        end
    else
        for k,v in ipairs(carriersIds) do
            if UnitExists(v) then
                self.carriers[3 - k].health = UnitHealth(v)/UnitHealthMax(v) * 100
            else
                self.carriers[3 - k].health = nil
            end
        end
    end
    local time = GetAreaSpiritHealerTime()
    if lastSpiritHealerTime and lastSpiritHealerTime ~= time and time > 1 and time < 30 then
        lastSpiritHealerTime = time
        local off = self:spiritHealerTime() - time -1
        if math.abs(off) > .25 then
            ressoff = ressoff + off
            self.b:restart(self)
        end
    elseif time ~= 0 then
        lastSpiritHealerTime = time
    else
        lastSpiritHealerTime = nil
    end
end
